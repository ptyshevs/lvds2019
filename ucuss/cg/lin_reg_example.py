import torch
import numpy as np


trX = np.linspace(-1, 1, 101)
trY = 2 * trX + np.random.randn(*trX.shape) * 0.2


w = torch.tensor(-2.0, requires_grad=True)
w_trace = []
for i in range(100):
    w_trace.append(w.item())
    for tr_x, tr_y in zip(trX, trY):
        x = torch.tensor(tr_x)
        y = torch.tensor(tr_y)
        y_hat = x * w
        cost = torch.pow(y_hat - y, 2)

        # w_grad = torch.autograd.grad(cost, w)[0]
        # w.data -= 0.0005 * w_grad.data

        cost.backward()
        w.data -= 0.0008 * w.grad.data
        w.grad.data.zero_()
w_trace.append(w.item())
print(w.item())


# ========================================================================
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import animation


fig = plt.figure()
ax = plt.axes(xlim=(-1.5, 1.5), ylim=(-3, 3))
line, = ax.plot([], [], 'r', lw=2)
data, = ax.plot(trX, trY, 'bo')


def init():
    line.set_data([], [])
    return line, data


def animate(i):
    line.set_data(trX, w_trace[i] * trX)
    ax.set_title('current slope %0.2f' % w_trace[i])
    return line, ax


anim = animation.FuncAnimation(fig, animate, init_func=init, frames=100)
with open('lin_reg_video.mp4', 'w') as f:
    anim.save(f.name, fps=20, extra_args=['-vcodec', 'libx264'])