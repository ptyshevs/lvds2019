import torch
import numpy as np


x = np.array([0.5, 0.2, 0.3])
A = np.ones((3, 3))
b = np.array([0.1, 0.2, 0.3])
c = np.array([1.0, 1.0, 1.0])

x = torch.tensor(x, requires_grad=True)
A = torch.tensor(A)
b = torch.tensor(b)
c = torch.tensor(c)

y = A.mv(x) + x * b + c
L = y.sum()

L.backward()

print(x.grad)
