import numpy as np


def f(x):
    return np.exp(-x ** 2 / 2)


def sample(N):
    """
    This function returns samples from the uniform(0, 1) distribution
    """
    return np.random.rand(N)


N = 10
estimate = []
for i in range(1000):
    f_values = f(sample(N))
    estimate.append(np.mean(f_values))
std = np.std(estimate)
mean = np.mean(estimate)
print(mean, std)