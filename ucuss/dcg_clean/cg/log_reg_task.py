import torch
import numpy as np


def model(x, w):
    """
    x: input
    w: weight
    Returns expression of logistic regression
    """
    raise NotImplementedError


def get_cost(y, y_hat):
    """
    y: variable that represents the true output value
    y_hat: variable that represents the predicted output value
    Returns expression of cross entropy
    """
    raise NotImplementedError


def sgd_step(w, lr):
    """

    w: model's parameter
    w_grad: gradient of the loss function w.r.t. w
    lr: learning rate
    Performs SGD update of the parameter `w` using `w.grad`
    """
    raise NotImplementedError


trX = np.linspace(-1, 1, 101)
trY = np.concatenate((np.random.choice([0, 1], size = 51, p=[0.8, 0.2]),
                      np.random.choice([0, 1], size = 50, p=[0.2, 0.8]))).astype(np.float64)
w = torch.tensor(0.0, requires_grad=True)
w_trace = []
for i in range(100):
    w_trace.append(w.item())
    for tr_x, tr_y in zip(trX, trY):
        x = torch.tensor(tr_x)
        y = torch.tensor(tr_y)

        # Write here forward and backward pass
        raise NotImplementedError

        w.grad.data.zero_()
w_trace.append(w.item())
print(w.item())



# ========================================================================
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import animation


fig = plt.figure()
ax = plt.axes(xlim=(-1.2, 1.2), ylim=(-0.25, 1.25))
line, = ax.plot([], [], 'r', lw=2)
data, = ax.plot(trX, trY, 'bo')


def init():
    line.set_data([], [])
    return line, data


def animate(i):
    line.set_data(trX, 1.0 / (1.0 + np.exp(-w_trace[i] * trX)))
    ax.set_title('current slope %0.2f' % w_trace[i])
    return line, ax


anim = animation.FuncAnimation(fig, animate, init_func=init, frames=100)
with open('log_reg_video.mp4', 'w') as f:
    anim.save(f.name, fps=20, extra_args=['-vcodec', 'libx264'])