import torch
import numpy as np


def f(x, c=4.):
    return x ** 2 + c


def sample_gaussian_noise(mu, log_sigma, size, reparameterise=False):
    # Sample Gaussian noise and return it.
    # Implement both parameterisable and non reparameterisable sampling.
    sigma = log_sigma.exp()
    if reparameterise:
        noise = torch.empty((size,), dtype=mu.dtype)
        return mu + sigma * noise.normal_()
    else:
        with torch.no_grad():
            return torch.normal(mu.expand(size), sigma.expand(size))


def log_prob(samples, mu, log_sigma):
    # Return Gaussian(mu, sigma) log prob of samples
    sigma = log_sigma.exp()
    return -(sigma * np.sqrt(2 * np.pi)).log() - (samples - mu).pow(2) / (2 * sigma.pow(2))


def get_params(mu_init, log_sigma_init):
    # Return mu and sigma parameters with initial values mu_init, log_sigma_init
    mu = torch.tensor(mu_init, requires_grad=True)
    log_sigma = torch.tensor(log_sigma_init, requires_grad=True)
    return mu, log_sigma


def sgd_step(w, lr):
    """
    w: model's parameter
    lr: learning rate
    Performs SGD update of the parameter `w` using `w.grad`
    """
    w.data -= lr * w.grad


torch.manual_seed(42)

learning_rate = 0.001
batch_size = 2
max_num_updates = 30000


# Path-wise derivative estimator
mu, log_sigma = get_params(1.0, 0.0)
for i in range(max_num_updates):
    samples = sample_gaussian_noise(mu, log_sigma, batch_size, True)

    # implement loss for path-wise derivative estimator
    loss = f(samples).mean()

    loss.backward()
    sgd_step(mu, learning_rate)
    sgd_step(log_sigma, learning_rate)

print(mu.item())
print(log_sigma.exp().item())

# score function estimator
mu, log_sigma = get_params(1.0, 0.0)
for i in range(max_num_updates):
    samples = sample_gaussian_noise(mu, log_sigma, batch_size, False)

    # implement loss for score function estimator
    loss = (f(samples) * log_prob(samples, mu, log_sigma)).mean()
    loss.backward()
    sgd_step(mu, learning_rate)
    sgd_step(log_sigma, learning_rate)


print(mu.item())
print(log_sigma.exp().item())