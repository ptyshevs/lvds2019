import imageio
import numpy as np
import os
import pickle
import shutil


def unpickle(fn):
    import pickle
    with open(fn, 'rb') as fo:
        d = pickle.load(fo, encoding='bytes')
    return d

def dopickle(obj, fn):
    import pickle
    with open(fn, 'wb') as fo:
        pickle.dump(obj, fo)

def convert_single_batch(batch, lbls_mapping, subset_name,
                        trg_subset_img_dirs, trg_anno_fns):
    imgs_in_batch = len(batch[b'filenames'])
    batch_images = [[] for _ in range(imgs_in_batch)]
    batch_labels = [[] for _ in range(imgs_in_batch)]
    
    for img_idx in range(imgs_in_batch):
        img = batch[b'data'][img_idx]
        img_fn = batch[b'filenames'][img_idx].decode("utf-8")
        img = img.reshape([3, 32, 32]).swapaxes(0, 1).swapaxes(1, 2)
        lbl_idx = batch[b'labels'][img_idx]
        batch_images[img_idx] = img
        batch_labels[img_idx] = lbl_idx
        
        lbl = lbls_mapping[b'label_names'][lbl_idx].decode("utf-8")
        img_full_fn = os.path.join(lbl, img_fn)
        imageio.imwrite(os.path.join(trg_subset_img_dirs[subset_name], img_full_fn), img)
        with open(trg_anno_fns[subset_name], 'a') as f:
            f.write('{}\t{}\n'.format(img_full_fn, lbl_idx))
            
    return batch_images, batch_labels

            
def convert(raw_dir, trg_dir):
    #in
    test_batch_fn = os.path.join(raw_dir, 'test_batch')
    batches_fns = [os.path.join(raw_dir, fn) for fn in os.listdir(raw_dir) if fn.startswith('data')]
    meta_data_fn = os.path.join(raw_dir, 'batches.meta')
    #out
    trg_subset_img_dirs = {
        'train': os.path.join(trg_dir, 'images/train/'),
        'test':  os.path.join(trg_dir, 'images/test/')
    }

    trg_lbl_dir = os.path.join(trg_dir, 'annotations')
    trg_anno_fns = {
        'train': os.path.join(trg_lbl_dir, 'train.txt'),
        'test': os.path.join(trg_lbl_dir, 'test.txt'),
    }


    if os.path.isdir(trg_subset_img_dirs['train']):
       shutil.rmtree(trg_subset_img_dirs['train'])
    if os.path.isdir(trg_subset_img_dirs['test']):
       shutil.rmtree(trg_subset_img_dirs['test'])
    if os.path.isdir(trg_lbl_dir):
       shutil.rmtree(trg_lbl_dir)

    meta_data = unpickle(meta_data_fn)

    for lbl in meta_data[b'label_names']:
        os.makedirs(os.path.join(trg_subset_img_dirs['train'], lbl.decode("utf-8")))
        os.makedirs(os.path.join(trg_subset_img_dirs['test'], lbl.decode("utf-8")))
    os.makedirs(trg_lbl_dir)

    test_batch = unpickle(test_batch_fn)
    bimgs, blbls = convert_single_batch(test_batch, meta_data, 'test', trg_subset_img_dirs, trg_anno_fns)
    test_set = {'images': bimgs, 'labels': blbls}
    print('Test batch was converted')
    
    train_set = {'images': [], 'labels': []}
    for i, fn in enumerate(sorted(batches_fns)):
        batch = unpickle(fn)
        bimgs, blbls = convert_single_batch(batch, meta_data, 'train', trg_subset_img_dirs, trg_anno_fns)
        train_set['images'].extend(bimgs)
        train_set['labels'].extend(blbls)
        print('Train batch {} of {} was converted'.format(i+1, len(batches_fns)))
        
    dopickle(train_set, os.path.join(trg_dir, 'train.pkl'))
    dopickle(test_set, os.path.join(trg_dir, 'test.pkl'))

