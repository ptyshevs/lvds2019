---
title: "CLT in action"
output: html_notebook
---


### Discrete random variables

How good is normal approximation for coin tosses?

If a coin is not fair, then $\mu = p$ and $\sigma^2 = p(1-p)$

```{r}
normal.approx <- function(n, p){
  s <- sqrt(p*(1-p))
  tosses <- replicate(10000, mean(rbinom(n,1,p)))
  norm.tosses <- sqrt(n)*(tosses - p)/s
  plot(ecdf(norm.tosses), ylab = "ecdf", col = "darkblue", lwd =2, main = paste("Normal approximation of the mean of", n, "tosses", sep = " "))
  x <-seq(-3,3, by = .01)
  lines(x, pnorm(x), col = "red", lwd = 2)
}
```


```{r}
normal.approx(n = 10, p =.5)
```
### Continuous random variables

How good is normal approximation for samples from the uniform distribution over $(0,1)$?

$\mu = .5$ and $\sigma^2 = 1/12$

```{r}
normal.approx.c <- function(n){
  s <- sqrt(1/12)
  tosses <- replicate(10000, mean(runif(n)))
  norm.tosses <- sqrt(n)*(tosses - .5)/s
  plot(ecdf(norm.tosses), ylab = "ecdf", col = "darkblue", lwd =2, main = paste("Normal approximation of the mean of", n, "uniform rv", sep = " "))
  x <-seq(-3,3, by = .01)
  lines(x, pnorm(x), col = "red", lwd = 2)
}
```


```{r}
normal.approx.c(n = 1)
```