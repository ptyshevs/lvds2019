---
title: 'Descriptive statistics: diabetes data'
output: 
  html_document: 
    toc: yes
editor_options: 
  chunk_output_type: inline
---

# 1. First look at the data

## The data

#### A population of women who were at least 21 years old, of *Pima Indian heritage* and living near Phoenix, Arizona, was tested for diabetes according to World Health Organization criteria. The data were collected by the **US National Institute of Diabetes and Digestive and Kidney Diseases**

#### Columns:

* `npreg`  
number of pregnancies.

* `glu`  
plasma glucose concentration in an oral glucose tolerance test.

* `bp`  
diastolic blood pressure (mm Hg).

* `skin`  
triceps skin fold thickness (mm).

* `bmi`   
body mass index (weight in kg/(height in m)\^2).

* `ped`  
diabetes pedigree function.

* `age`  
age in years.

* `type`  
**Yes** or **No**, for diabetic according to WHO criteria.

```{r}
library("MASS")
diabetes <- Pima.te
head(diabetes, 10)
```

```{r}
colnames(diabetes)    # names of the columns
dim(diabetes)         # number of rows and columns
str(diabetes)         # structure of the dataframe
```


#### Now we attach the dataframe to simplify usage of the variables 

```{r}
attach(diabetes)
```

```{r}
age
bmi
```

# 2. Getting to know the data

## Descriptive statistics


### Categorial (factor) variables: **type**

#### List the values and their frequencies

```{r}
table(type)      # list the values and their frequencies
summary(type)    # the same (without the var name)
```

#### Visualize these: 

```{r}
library(ggplot2)
ggplot(diabetes, aes(x = type, fill = type)) + 
  geom_bar()
ggplot(diabetes, aes(x = factor(1), fill = type)) + 
  geom_bar(width=1) +
  coord_polar("y" )
 
```


## Description of continuous variables

### Central tendency: mean, median, mode

`mean(data)`  returns the arithmetic mean

`media(data)` returns the value that splits the data in two halves

`mode(data)` gives the most frequent value; used for integer-valued or categorical data

```{r}
str(age)            #  structure of the variable age
mean(age)           #  the mean value
median(age)         #  the median
age_sorted <- sort(age)
age_sorted[length(age)/2]
sum(age<=median(age))
sum(age>=median(age))
table(age)
tabulate(age)
which.max(tabulate(age))    # the mode of the age 
```


### Measure of spread: variance, standard deviation, min/max, spread

For data $x_1,\dots,x_n$  

* **mean** is $$ \overline{\mathbf{x}} =\mu = \frac1n\sum_{k=1}^n x_k$$  
* **variance** is 
$$ \operatorname{var}(\mathbf{x}) = \sigma^2= \frac1{n-1}\sum_{k=1}^n (x_k - \overline{\mathbf{x}})^2$$  
* **standard deviation** is 
$$\operatorname{sd}(\mathbf{x}) = \sigma = \sqrt{\operatorname{var}(\mathbf{x})}$$  
* **spread** is $\max(\mathbf{x}) - \min(\mathbf{x})$

```{r}
cat("Mean value: ", mean(bmi), sum(bmi)/length(bmi), "\n")
cat("variance: ", var(bmi), sum((bmi-mean(bmi))^2)/(length(bmi)-1), "\n")
cat("standard deviation: ", sd(bmi), sqrt(var(bmi)), "\n")
cat("min, max, spread: ", min(bmi), max(bmi), max(bmi) - min(bmi), "\n")
```

### Histograms

```{r}
ggplot(data=diabetes, aes(x=age)) +
  geom_histogram(binwidth = 2, color = "black", fill = "lightblue", alpha =0.5)
```

#### Is there any difference between the two groups?

```{r}
ggplot(data=diabetes, aes(x=age)) +
  geom_histogram(binwidth = 2, alpha= .5, color = "black",  fill = "lightblue") + 
  facet_grid(cols = vars(type))
```

#### We can overlay the two histograms:

```{r}
ggplot(data=diabetes, aes(x=age, color = type, fill = type)) +
  # position can be "identity", "stack", or "dodge"
  geom_histogram(binwidth = 1, alpha= .5, position="identity") 
```

#### What about the Body Mass Index? 

```{r}
ggplot(data=diabetes, aes(x=bmi)) +
  geom_histogram(binwidth = 2, color = "black", fill = "lightblue")
```

#### Overlay the density plot

```{r}
ggplot(data=diabetes, aes(x=bmi)) +
  geom_histogram(aes(y = ..density..), binwidth = 2, color = "black", fill = "lightblue") +
  geom_density(alpha=.2, fill="white") 
```

#### Fit the "standard" density curve

```{r}
ggplot(data=diabetes, aes(x=bmi)) +
  geom_histogram(aes(y = ..density..), binwidth = 2, color = "black", fill = "lightblue") +
  geom_density(alpha=.2, fill="white") +
  stat_function(fun = dnorm, args = list(mean = mean(bmi), sd = sd(bmi)), color = "red", lty = 1) + 
  geom_vline(xintercept = mean(bmi), color = "blue")
```

#### Separately by type


```{r}
bmi_hist_over <- ggplot(data=diabetes, aes(x=bmi, color = type, fill = type)) +
  geom_histogram(binwidth = 2, alpha= .5, position="identity")
bmi_hist_over
```


```{r}
library(plyr)
mu <- ddply(diabetes, "type", summarise, grp.mean=mean(bmi))
sigma <- ddply(diabetes, "type", summarise, grp.sd=sd(bmi))
ggplot(data=diabetes, aes(x=bmi, color = type, fill = type)) +
  geom_histogram(aes(y = ..density..), binwidth = 2, alpha= .5, position="identity") +
  geom_vline(data= mu, aes(xintercept=grp.mean, color = type), linetype="dashed", size = 1) +
  geom_density(alpha=.2) +
  stat_function(fun = dnorm, args = list(mean = mu[1,2], sd = sigma[1,2]), color = "red") + 
  stat_function(fun = dnorm, args = list(mean = mu[2,2], sd = sigma[2,2]), color = "blue")

```

### Shape of the distribution

```{r}
library(moments)
```

$k$-th central moment ($m_2 = \sigma^2$): 
$$m_k:=\frac1n\sum_{j=1}^n (x_j-\overline{\mathbf{x}})^k$$


* **Skewness**: is it symmetric? 
$$ s=\frac{m_3}{m_2^{3/2}} $$  
  + $s>0$  *positively skewed*, heavy *right* tail
  + $s<0$  *negatively skewed*, heavy *left* tail
  
* **kurtosis**: is it too peaky? 
$$k=\frac{m_4}{m_2^2}-3$$
  + $k>0$  *flat*  
  + $k<0$  *peaky*

Recall the data distribution: 

```{r}
ggplot(data=diabetes, aes(x=bmi)) +
  geom_histogram(aes(y = ..density..), binwidth = 1, color = "black", fill = "lightblue") +
  stat_function(fun = dnorm, args = list(mean = mean(bmi), sd = sd(bmi)), color = "red")
```



```{r}
# define a function for skewness
#
skew <- function (x) mean((x-mean(x))^3) / (mean((x-mean(x))^2))^(3/2)
#
skew(bmi)
skew(bmi[bmi<49])
skew(bmi[type == "No" & bmi<49])
#
# define a function for kurtosis
#
kurt <- function(x) mean((x-mean(x))^4)/ (mean((x-mean(x))^2))^2 - 3
#
kurt(bmi)
kurt(bmi[type == "No"])

skew(rnorm(1000))
kurt(rnorm(1000))
```


### Quartiles

First and third quartiles split off lower and upper quarters of the data

```{r}
quantile(bmi)
sort(bmi)[length(bmi)/4]
quantile(bmi, probs = .25)

for (p in c(.25,.5,.75)) 
cat("Probability for p=", p, "is", mean(bmi < quantile(bmi, probs = p)), sep= " ", "\n")
summary(bmi)
```


Boxplot gives a geometric representation of the summary:


```{r}
ggplot(diabetes, aes(x = "Boxplot of bmi", y = bmi)) + 
  geom_boxplot(color = "black", fill = "lightblue", width = .5)


# The same w.r.t. type: 

ggplot(diabetes, aes(type, bmi)) + 
  geom_boxplot(aes(color = type, fill = type), alpha=.5)

##  the same: 
#  ggplot(diabetes, aes(x = type, y = bmi, color= type, fill = type)) +
#  geom_boxplot(alpha=.5)
``` 
### Empirical cumulative distribution function

**Ecdf** $\hat{F}_{\mathbf{x}}(t)$ characterizes distribution of the values $x_1,\dots,x_n$:
		\[
			\hat{F}_{\mathbf{x}}(t) = \frac{\#\{k \,:\, x_k <t\}}{n}
		\]
		
```{r}
plot(ecdf(rnorm(10)), col = "blue")

plot(ecdf(bmi), main = "Ecdf of bmi", col = "blue", pch = 1, xlab = "t", ylab = "F(t)")
pts <- seq(-30,70, by=.1)
lines(pts, pnorm(pts, mean = mean(bmi), sd = sd(bmi)), col = "red", lwd = 2)

```
#### Let's have a look at the ecdf of **age**:
		
```{r}
plot(ecdf(age), main = "Ecdf of age", col = "blue", pch = 1, xlab = "t", ylab = "F(t)")
pts <- seq(10,90, by=.5)
lines(pts, pnorm(pts, mean = mean(age), sd = sd(age)), col = "red", lwd = 2)

```

### k sigma rule

#### Claims that inside the $k\cdot \sigma$ distance from the mean ($k=1,2,3$) there are $p = .68, .95, .99$ fractions of the (normally distributed) values

```{r}
for (k in (1:3)) {
  p <-  mean(abs(bmi - mean(bmi)<k*sd(bmi)))
  cat("There are", round(p*100,2), "per cent values within", k, "standard deviations of the mean of bmi", sep = " ", "\n")
}  
       
``` 

```{r}
for (k in (1:3)) {
  p <-  mean(abs(age - mean(age)<k*sd(age)))
  cat("There are", round(p*100,2), "per cent values within", k, "standard deviations of the mean of age", sep = " ", "\n")
}  
``` 

#### Normality assumption is important! 

```{r}
exp_data <- rexp(500, .25)
plot(ecdf(exp_data), main = "Ecdf of exp_data", col = "blue", pch = 1, xlab = "t", ylab = "F(t)")
pts <- seq(-4,100, by=.05)
lines(pts, pnorm(pts, mean = mean(exp_data), sd = sd(exp_data)), col = "red", lwd = 2)

```

```{r}
for (k in (1:3)) {
  p <-  mean(abs(exp_data - mean(exp_data)<k*sd(exp_data)))
  cat("There are", round(p*100,2), "per cent values within", k, "standard deviations of the mean of exp_data", sep = " ", "\n")
}  
``` 

# 3 Assignment: Analyze the variable **ped**

* Is there anything special about the distribution of its values?  
* Is the distribution different within the two classes (`type = Yes` and `type=No`)?  
* Characterize the distribution location, spread, shape etc
* Do you think this variable can be helpful to predict diabetes?


