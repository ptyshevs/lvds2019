---
title: "Two envelopes"
output:
  html_notebook:
    fig_caption: yes
    number_sections: no
    toc: yes
  html_document: default
editor_options:
  chunk_output_type: inline
---

## The game
#### There are two envelopes with money, and one of them contains twice as much as the other. The player chooses one of the envelopes at random, and then the host suggests to change it for the other. Should one change?


### Realistic: It does not matter

#### If the amounts in the envelopes are $X$ and $2X$, then by changing the player either increases the amoun by $X$, or decreases it by $X$, both with probability $\tfrac12$ 

Here is a simulation of the game:


```{r}
n <- 1e3  # no of games
x <- abs(round(rnorm(n, mean = runif(n,-10,10), sd = 10), digits = 2))  # numbers X generated
x1 <- x * sample(c(1,2), n, replace = T)   # given to the first player
x2 <- 3*x  - x1                             # given to the second player
two.envelopes <- data.frame(first = x1, second = x2)
rbind(head(two.envelopes,5), tail(two.envelopes,5))
```

```{r}
mean.gain.first <- mean(x1) 
mean.gain.second <- mean(x2)
cat("Mean amounts in the envelopes are", mean.gain.first, mean.gain.second, sep = " ")
```


### Optimitic: One should change! 

#### If the amount in the chosen envelope is $Y$, then the other contains $2Y$ or $Y/2$, both with probability $\tfrac12$. By swapping, the player gets on average $\tfrac12(2Y + Y/2) = \tfrac54 Y > Y$

```{r}
chosen <- x   # sum in the envelope chosen
swapped <- chosen*sample(c(.5,2), n, replace = T)       # sum in theotherenvelope
two.envelopes1 <- data.frame(first = chosen, second = swapped)
rbind(head(two.envelopes1,5), tail(two.envelopes1,5))
```

```{r}
cat("Mean amounts in the envelopes are", mean(chosen), mean(swapped), "\n", sep = " ")
cat("Ratio of the means:", mean(swapped)/mean(chosen), sep = " ")

```
# Has the probability theory got broken here?!! 
